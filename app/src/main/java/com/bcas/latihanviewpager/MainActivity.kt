package com.bcas.latihanviewpager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bcas.latihanviewpager.databinding.ActivityMainBinding
import com.bcas.latihanviewpager.fragment.ChatFragment
import com.bcas.latihanviewpager.fragment.StatusFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var myViewPagerAdapter: MyViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val chatFragment = ChatFragment()
        val statusFragment = StatusFragment()

        myViewPagerAdapter = MyViewPagerAdapter(supportFragmentManager)

        myViewPagerAdapter.addFragment(chatFragment, "Chat")
        myViewPagerAdapter.addFragment(statusFragment, "Status")
        myViewPagerAdapter.addFragment(statusFragment, "Call")

        binding.vpMain.adapter = myViewPagerAdapter
        binding.tlMain.setupWithViewPager(binding.vpMain)
    }
}